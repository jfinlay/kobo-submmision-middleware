from flask import Flask, request, flash, session
from flask_httpauth import HTTPBasicAuth
from flask_cors import CORS
import os
from werkzeug.utils import secure_filename
import json
import commands

auth = HTTPBasicAuth()
app = Flask(__name__)
CORS(app)

UPLOAD_FOLDER = '/tmp'
ALLOWED_EXTENSIONS = {'xml'}

@auth.verify_password
def authenticate(username, password):
    if username and password:
        if username == 'user' and password == 'pass':
            return True
        else:
            return False
    return False


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route("/submissions", methods=['POST'])
@auth.login_required
def receive_submission():
    # setup here your kobo or ODK endpoint
    cmd = 'curl -X POST -F "xml_submission_file=@{};type=text/xml" '\
        'https://kc.humanitarianresponse.info/api/v1/submissions '\
        '-u KOBOUSER:KOBOPASSWORD'
    print("INFO: {}".format(request.method))
    # check if the post request has the file part
    if 'file' not in request.files:
        flash('No file part')
        return "ERROR: No se ha encontrado un archivo"
    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        flash('No selected file')
        return "ERROR: Debe definir un nombre para el archivo"
    filename = secure_filename(file.filename)
    print filename
    file.save(os.path.join(UPLOAD_FOLDER, filename))
    cmd = cmd.format(os.path.join(UPLOAD_FOLDER, filename))
    returned_value = os.system(cmd)
    return "SUCCESS: {}".format(returned_value)


@app.route("/submissionscount", methods=['GET'])
@auth.login_required
def query_submission():
    url = str(request.args.get('url'))
    cmd = 'curl -X GET "https://kc.humanitarianresponse.info/api/v1/data/600831?query=%7B%22url%22:%22{}%22%7D"'\
        ' -u KOBOUSER:KOBOPASSWORD 2>/dev/null'.format(url)
    status, res = commands.getstatusoutput(cmd)
    res = json.loads(res)
    return "{}".format(len(res))


if __name__ == "__main__":

    app.secret_key = b'your super secret key'
    app.run()
